# CyclomaticComplexity

#### 介绍
# 目的 
构建一个独立的针对C、Java源码语言的圈复杂度计算程序，支持输入文件与路径。

#### 软件架构
软件架构说明参考Doc中的设计文档。

#### 系统支持

目前该模块的开发环境为Ubuntu20.04 64位系统，在该系统下进行了测试，尚未在其他系统平台进行测试。

#### 安装教程

1.  通过pip install MetricTool安装

#### 使用说明

1.  安装完成后，自带了测试用例，执行测试用例命令如下：
```
python -m MetricTools.test_PyCyclomatic
python -m MetricTools.test_C_visitor
python -m MetricTools.test_Java_visitor
python -m MetricTools.test_Cyclomatic
python -m MetricTools.test_File_visit
python -m MetricTools.test_Parallel_cyclomatic
```
2.  直接使用模块对文件或文件夹进行圈复杂度分析，命令如下：
```
python -m MetricTools.PyCyclomatic file/directory
```
3.  通过导入项目包使用，示例如下：
```
import MetricTools.PyCyclomatic as p_c

file_s = "file or directory"
result = p_c.CyclomaticAnalysis(file_s)
```

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
